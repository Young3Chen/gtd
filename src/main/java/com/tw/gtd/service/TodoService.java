package com.tw.gtd.service;

import com.tw.gtd.Exception.TodoNotFoundException;
import com.tw.gtd.dto.TodoRequest;
import com.tw.gtd.dto.TodoResponse;
import com.tw.gtd.entity.Todo;
import com.tw.gtd.mapper.TodoMapper;
import com.tw.gtd.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.tw.gtd.mapper.TodoMapper.todoResponse;
import static java.util.stream.Collectors.toList;

@Service
public class TodoService {

    @Autowired
    private TodoRepository todoRepository;

    public List<TodoResponse> list() {
        return todoRepository.findAll().stream()
                .map(TodoMapper::todoResponse)
                .collect(toList());
    }

    public TodoResponse getById(Integer id) {
        return todoResponse(todoRepository.findById(id).orElseThrow(TodoNotFoundException::new));
    }

    public TodoResponse updateById(Integer id, TodoRequest todoRequest) {
        Todo todoFound = todoRepository.findById(id)
                .orElseThrow(TodoNotFoundException::new);
        if (todoRequest.getText() != null) {
            todoFound.setText(todoRequest.getText());
        }
        if (todoRequest.isDone() != null) {
            todoFound.setDone(todoRequest.isDone());
        }
        return todoResponse(todoRepository.save(todoFound));
    }

    public TodoResponse createTodo(TodoRequest todoRequest) {
        Todo todo = TodoMapper.toEntity(todoRequest);
        return todoResponse(todoRepository.save(todo));
    }

    public void deleteById(Integer id) {
        todoRepository.deleteById(id);
    }
}
