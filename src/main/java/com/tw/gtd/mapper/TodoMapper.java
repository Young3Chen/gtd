package com.tw.gtd.mapper;

import com.tw.gtd.dto.TodoRequest;
import com.tw.gtd.dto.TodoResponse;
import com.tw.gtd.entity.Todo;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static TodoResponse todoResponse(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

    public static Todo toEntity(TodoRequest todoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest, todo);
        return todo;
    }
}