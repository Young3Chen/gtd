package com.tw.gtd.controller;

import com.tw.gtd.dto.TodoRequest;
import com.tw.gtd.dto.TodoResponse;
import com.tw.gtd.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    @Autowired
    private TodoService todoService;
    @GetMapping
    public List<TodoResponse> list(){
        return todoService.list();
    }

    @GetMapping("/{id}")
    public TodoResponse getById(@PathVariable Integer id){
        return todoService.getById(id);
    }

    @PutMapping("/{id}")
    public TodoResponse updateById(@PathVariable Integer id, @RequestBody TodoRequest todoRequest){
        return todoService.updateById(id,todoRequest);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse createTodo(@RequestBody TodoRequest todoRequest){
        return todoService.createTodo(todoRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable Integer id){
        todoService.deleteById(id);
    }

}
