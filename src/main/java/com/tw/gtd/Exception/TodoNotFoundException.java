package com.tw.gtd.Exception;

public class TodoNotFoundException extends RuntimeException{
    public TodoNotFoundException(){
        super("TodoNotFound");
    }
}
