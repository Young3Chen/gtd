package com.tw.gtd;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tw.gtd.dto.TodoRequest;
import com.tw.gtd.entity.Todo;
import com.tw.gtd.repository.TodoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ToDoControllerTest {
    @Autowired
    private MockMvc client;

    @Autowired
    TodoRepository todoRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void prepareData() {
        todoRepository.deleteAll();
    }

    @Test
    void should_return_todos_preform_get_given_todos_in_db() throws Exception {
        //given
        Todo readBook = new Todo("read book", false);
        Todo doSomeSport = new Todo("do some sport", false);

        //when then
        todoRepository.saveAll(List.of(readBook, doSomeSport));
        client.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(readBook.getText()))
                .andExpect(jsonPath("$[0].done").value(readBook.isDone()))
                .andExpect(jsonPath("$[1].text").value(doSomeSport.getText()))
                .andExpect(jsonPath("$[1].done").value(doSomeSport.isDone()));
    }

    @Test
    void should_return_right_todo_perform_get_given_todos_in_db_and_a_todo_id() throws Exception {
        //given
        Todo readBook = new Todo("read book", false);

        //when then
        Todo savedTodo = todoRepository.save(readBook);
        client.perform(MockMvcRequestBuilders.get("/todos/{id}", savedTodo.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(savedTodo.getId()))
                .andExpect(jsonPath("$.text").value(savedTodo.getText()))
                .andExpect(jsonPath("$.done").value(savedTodo.isDone()));
    }


    @Test
    void should_update_todo_perform_put_given_a_todo_with_id() throws Exception {
        //give
        Todo workHard = new Todo("work hard", false);
        TodoRequest workHardNew = new TodoRequest("work hard!!!!", true);
        Todo savedTodo = todoRepository.save(workHard);
        String workHardNewString = objectMapper.writeValueAsString(workHardNew);

        //when
        client.perform(MockMvcRequestBuilders.put("/todos/{id}", savedTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(workHardNewString))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(workHardNew.getText()))
                .andExpect(jsonPath("$.done").value(workHardNew.isDone()));

        //then
        Todo toDObyId = todoRepository.findAll().get(0);
        Assertions.assertEquals(savedTodo.getId(), toDObyId.getId());
        Assertions.assertEquals(workHardNew.getText(), toDObyId.getText());
        Assertions.assertEquals(workHardNew.isDone(), toDObyId.isDone());
    }

    @Test
    void should_create_todo_perform_post_given_a_todo() throws Exception {
        //given
        Todo workHard = new Todo("work hard", false);
        String workHardTodoString = objectMapper.writeValueAsString(workHard);

        //when
        client.perform(MockMvcRequestBuilders.post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(workHardTodoString))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(jsonPath("$.text").value(workHard.getText()))
                .andExpect(jsonPath("$.done").value(workHard.isDone()));

        //then
        Todo todoFound = todoRepository.findAll().get(0);
        Assertions.assertEquals(workHard.getText(), todoFound.getText());
        Assertions.assertEquals(workHard.isDone(), todoFound.isDone());
    }

    @Test
    void should_delete_todo_perform_delete_given_todo() throws Exception {
        // given
        Todo savedTodo = todoRepository.save(new Todo("just do it", false));

        // when
        client.perform(MockMvcRequestBuilders.delete("/todos/{id}", savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        // then
        assertThat(todoRepository.findAll(), empty());
    }

    @Test
    void should_throw_NotFountException_perform_get_given_no_saved_id() throws Exception {
        // given
        Todo savedTodo = todoRepository.save(new Todo("just do it", false));
        todoRepository.deleteById(savedTodo.getId());

        // when then
        client.perform(MockMvcRequestBuilders.get("/todos/{id}", savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void should_throw_NotFountException_perform_update_given_no_saved_id() throws Exception {
        // given
        Todo savedTodo = todoRepository.save(new Todo("just do it", false));
        todoRepository.deleteById(savedTodo.getId());
        String savedTodoJson = objectMapper.writeValueAsString(savedTodo);

        // when then
        client.perform(MockMvcRequestBuilders.put("/todos/{id}", savedTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(savedTodoJson))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
